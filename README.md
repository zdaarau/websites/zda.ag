# zda.ag

[![Netlify Status](https://api.netlify.com/api/v1/badges/b151f738-6177-4587-adee-6bf16a5e1807/deploy-status)](https://app.netlify.com/sites/zda-ag/deploys)

...

## Shared assets

### Counter.dev JS

To update the script, run (from the root of the repo):

``` sh
curl --silent https://cdn.counter.dev/script.js | minify --type=js | sd '\s*$' '\n' > public/counter-dev.min.js
```

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[LICENSE.md](LICENSE.md).
